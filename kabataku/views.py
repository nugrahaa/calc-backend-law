from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Perhitungan
from .serializers import PerhitunganSerializer

class Kabataku(APIView):
    def get(self, request, format=None):
        perhitungans = Perhitungan.objects.all()
        perhitungan_serializer = PerhitunganSerializer(perhitungans, many=True)
        return Response(perhitungan_serializer.data)   

    def post(self, request, format=None):
        tipe = request.data['tipe']
        

        if tipe == '+':
            request.data['ans'] = request.data['a'] + request.data['b']
        elif tipe == '-':
            request.data['ans'] = request.data['a'] - request.data['b']
        elif tipe == '*':
            request.data['ans'] = request.data['a'] * request.data['b']
        elif tipe == '/':
            request.data['ans'] = request.data['a'] / request.data['b']

        perhitungan_serializer = PerhitunganSerializer(data=request.data)

        if perhitungan_serializer.is_valid():
            perhitungan_serializer.save()
            return Response(perhitungan_serializer.data, status=status.HTTP_201_CREATED)
            
        return Response(perhitungan_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
