from django.urls import path
from . import views

urlpatterns = [
    path('', views.Kabataku.as_view())
]
