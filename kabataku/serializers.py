from rest_framework import serializers
from .models import Perhitungan

class PerhitunganSerializer(serializers.ModelSerializer):
    class Meta:
        model = Perhitungan
        fields = '__all__'
